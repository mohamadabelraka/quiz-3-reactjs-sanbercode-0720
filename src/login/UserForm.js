import React, {useContext, useState} from 'react'
import {UserContext} from './UserContext'
import { useHistory } from 'react-router'
import "../movie/movie.css"


const UserForm = () =>{
  const [user, setUser] = useContext(UserContext)
  const [input, setInput] = useState({username: "", password: ""})
  const history = useHistory()

  const handleChange = (event) =>{
    let typeOfInput = event.target.name

    switch (typeOfInput){
      case "username":
      {
        setInput({...input, username: event.target.value});
        break
      }
      case "password":
      {
        setInput({...input, password: event.target.value});
        break
      }
    default:
      {break;}
    }
  }

  const handleSubmit = (event) =>{
    // menahan submit
    event.preventDefault()

    let username = input.username
    let password = input.password


    if (username.replace(/\s/g,'') !== "" && password.replace(/\s/g,'') !== ""){
      if (username==="admin" && password==="admin123"){
        setUser({isLogin : true})
        alert(`Login Berhasil, Hai ${input.username} !`)
        history.push("/")
      }
      else {
        alert("Username atau Password Salah")
      }
    }

  }
  return(
    <>
      <h1>Login</h1>

      <div id="formMovie" style={{width: "20%", margin: "0 auto", display: "block"}}>
        <div style={{border: "1px solid #aaa", padding: "20px"}}>
          <form onSubmit={handleSubmit}>
            <label style={{float: "left"}}>
              Username:
            </label>
            <input style={{float: "right"}} type="text" name="username" value={input.username} onChange={handleChange}/>
            <br/>
            <br/>
            <label style={{float: "left"}}>
              Password:
            </label>
            <input style={{float: "right"}} type="password" name="password" value={input.password} onChange={handleChange}/>
            <br/>
            <br/>
            <div style={{width: "100%", paddingBottom: "20px"}}>
              <button style={{ float: "right"}}>submit</button>
            </div>
          </form>
        </div>
      </div>
    </>
  )
}
export default UserForm
