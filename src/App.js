import React from 'react';
import {BrowserRouter as Router} from "react-router-dom";
import Nav from './dashboard/Nav';
import {UserProvider} from './login/UserContext'

function App() {
  return (
    <Router>
      <UserProvider>
        <Nav/>
      </UserProvider>
    </Router>
  );
}

export default App;
