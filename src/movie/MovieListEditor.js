import React, {useState, useEffect} from "react"
import axios from "axios"
import "./movie.css"
const MovieListEditor = () => {

  const [movieList, setMovieList] =  useState(null)
  const [input, setInput]  =  useState(
    {title: "", description: "", year: 2000, duration: 0, genre: "", rating: 1}
  )
  const [selectedId, setSelectedId]  =  useState(0)
  const [statusForm, setStatusForm]  =  useState("create")

  useEffect( () => {
    if (movieList === null){
      axios.get(`http://backendexample.sanbercloud.com/api/movies`)
      .then(res => {
        setMovieList(res.data.map(el=>{
          return {
            id: el.id, title: el.title,
            description: el.description, year: el.year,
            duration: el.duration, genre: el.genre,
            rating: el.rating
          }} ))
      })
    }
  }, [movieList])

  const handleDelete = (event) => {
    let idMovie = parseInt(event.target.value)

    let newMovieList = movieList.filter(el => el.id !== idMovie)

    axios.delete(`http://backendexample.sanbercloud.com/api/movies/${idMovie}`)
    .then(res => {
      console.log(res)
    })

    setMovieList([...newMovieList])

  }

  const handleEdit = (event) =>{
    let idMovie = parseInt(event.target.value)
    let listMovie = movieList.find(x=> x.id === idMovie)
    setInput({title: listMovie.title,
    description: listMovie.description, year: listMovie.year,
    duration: listMovie.duration, genre: listMovie.genre,
    rating: listMovie.rating})
    setSelectedId(idMovie)
    setStatusForm("edit")
  }

  const handleChange = (event) =>{
    let typeOfInput = event.target.name

    switch (typeOfInput){
      case "title":
      {
        setInput({...input, title: event.target.value});
        break
      }
      case "description":
      {
        setInput({...input, description: event.target.value});
        break
      }
      case "year":
      {
        setInput({...input, year: event.target.value});
          break
      }
      case "duration":
      {
        setInput({...input, duration: event.target.value});
          break
      }
      case "genre":
      {
        setInput({...input, genre: event.target.value});
          break
      }
      case "rating":
      {
        if(event.target.value >= 1 && event.target.value <= 10){
            setInput({...input, rating: event.target.value});
        }
          break
      }
    default:
      {break;}
    }
  }

  const handleSubmit = (event) =>{
    // menahan submit
    event.preventDefault()

    let title = input.title
    let description = input.description
    let genre = input.genre


    if (title.replace(/\s/g,'') !== "" && description.replace(/\s/g,'') !== "" && genre.replace(/\s/g,'') !== ""){
      if (statusForm === "create"){
        axios.post(`http://backendexample.sanbercloud.com/api/movies`, {title: input.title,
        description: input.description, year: input.year,
        duration: input.duration, genre: input.genre,
        rating: input.rating})
        .then(res => {
            setMovieList([
              ...movieList,
              { id: res.data.id,
                title: input.title,
                description: input.description, year: input.year,
                duration: input.duration, genre: input.genre,
                rating: input.rating
              }])
        })
      }else if(statusForm === "edit"){
        axios.put(`http://backendexample.sanbercloud.com/api/movies/${selectedId}`, {title: input.title,
        description: input.description, year: input.year,
        duration: input.duration, genre: input.genre,
        rating: input.rating})
        .then(() => {
            let listMovie = movieList.find(el=> el.id === selectedId)
            listMovie.title = input.title
            listMovie.description = input.description
            listMovie.year = input.year
            listMovie.duration = input.duration
            listMovie.genre = input.genre
            listMovie.rating = input.rating
            setMovieList([...movieList])
        })
      }

      setStatusForm("create")
      setSelectedId(0)
      setInput({title: "", description: "", year: 0, duration: 0, genre: "", rating: 0})
    }

  }

  return(
    <>

      <h1>Daftar Film Film Terbaik</h1>
      <table id = "tableMovie">
        <thead>
          <tr>
            <th>No</th>
            <th>Title</th>
            <th>description</th>
            <th>year</th>
            <th>duration</th>
            <th>genre</th>
            <th>rating</th>
            <th>Action</th>
          </tr>
        </thead>
        <tbody>

            {
              movieList !== null && movieList.map((item, index)=>{
                return(
                  <tr key={index}>
                    <td>{index+1}</td>
                    <td>{item.title}</td>
                    <td>{item.description}</td>
                    <td>{item.year}</td>
                    <td>{item.duration} minutes</td>
                    <td>{item.genre}</td>
                    <td>{item.rating}</td>
                    <td name="button">
                      <button onClick={handleEdit} value={item.id}>Edit</button>
                      &nbsp;
                      <button onClick={handleDelete} value={item.id}>Delete</button>
                    </td>
                  </tr>
                )
              })
            }
        </tbody>
      </table>
      {/* Form */}
      <h1>Form Edit Film</h1>

      <div style={{width: "50%", margin: "0 auto", display: "block"}} id ="formMovie">
        <div style={{border: "1px solid #aaa", padding: "20px"}}>
          <form onSubmit={handleSubmit}>
            <label style={{float: "left"}}>
              Title:
            </label>
            <input style={{float: "right"}} type="text" name="title" value={input.title} onChange={handleChange}/>
            <br/>
            <br/>
            <label style={{float: "left"}}>
              Description:
            </label>
            <textarea style={{float: "right"}} type="text" name="description" value={input.description} onChange={handleChange}/>
            <br/>
            <br/>
            <br/>
            <label style={{float: "left"}}>
              Year:
            </label>
            <input style={{float: "right"}} type="number" name="year" value={input.year} onChange={handleChange}/>
            <br/>
            <br/>
            <label style={{float: "left"}}>
              Duration (dalam menit):
            </label>
            <input style={{float: "right"}} type="number" name="duration" value={input.duration} onChange={handleChange}/>
            <br/>
            <br/>
            <label style={{float: "left"}}>
              Genre:
            </label>
            <input style={{float: "right"}} type="text" name="genre" value={input.genre} onChange={handleChange}/>
            <br/>
            <br/>
            <label style={{float: "left"}}>
              Rating:
            </label>
            <input style={{float: "right"}} type="number" name="rating" value={input.rating} onChange={handleChange}/>
            <br/>
            <br/>
            <div style={{width: "100%", paddingBottom: "20px"}}>
              <button style={{ float: "right"}}>submit</button>
            </div>
          </form>
        </div>
      </div>
    </>
  )
}

export default MovieListEditor
