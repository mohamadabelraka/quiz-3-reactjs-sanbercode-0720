import React, {useContext} from "react";
import {Link, Switch, Route, useHistory} from "react-router-dom";
import {UserContext} from '../login/UserContext';
import {Redirect} from "react-router-dom";
import "./dashboard.css"
import Logo from './img/logo.png'
import About from './About'
import Contact from './Contact'
import Dash from './Home'
import UserForm from '../login/UserForm'
import MovieListEditor from '../movie/MovieListEditor'


const Nav = () => {
  const history = useHistory()
  const [user, setUser] = useContext(UserContext)
    return (
      <>
        <header>
          <img id="logo" src={Logo} width="200px" />
          <nav>
            <ul>
              <li>
                <Link to="/">Home</Link>
              </li>
              <li>
                <Link to="/about">About</Link>
              </li>
              <li>
                <Link to="/contact">Contact</Link>
              </li>
              {user.isLogin ? (
              <>
                <li>
                  <Link to="/movie-editor">Movie List Editor</Link>
                </li>
                <li>
                  <div
                    onClick={() => {
                      setUser({ ...user, isLogin: false, userame: "" });
                      history.push("/");
                    }}
                    style={{ cursor: "pointer" }}
                  >
                    Logout
                  </div>
                </li>
              </>
            ) : (
              <li>
                <Link to="/login">
                  {user.isLogin ? `${user.userame}` : "Login"}
                </Link>
              </li>
            )}

            </ul>
          </nav>
        </header>

        <Switch>
        <Route path="/login">
          <UserForm />
        </Route>
        <Route path="/contact">
          <Contact />
        </Route>
          <Route path="/about">
            <About />
          </Route>
          <Route path="/movie-editor">
            <MovieListEditor />
          </Route>
          <Route exact path="/">
            <Dash />
          </Route>
        </Switch>
        <footer>
            <h5>copyright &copy; 2020 by Sanbercode</h5>
        </footer>
      </>
    )

}

export default Nav
